***HELP DESK***
Информационная система, предназначенная для решения проблем пользователей с компьютерами, периферийными устройствами и ПО.
Система позволяет пользователям создавать обращения в техническую поддержку с описанием своей проблемы. Специалисты, в свою очередь, могут обрабатывать эти обращения, решая возникшую неисправность;
Для начала работы необходимо:
- установить PostgeSQL
- в файле resources\application.property указать данные для подключения к БД
- аналогичные действия произвести в pom.xml в разделе:
<plugin>
<groupId>org.flywaydb</groupId>
<artifactId>flyway-maven-plugin</artifactId>
- Запустить Maven -> Plugins -> flyway -> flyway:clean
- Запустить Maven -> Plugins -> flyway -> flyway:migrate
- Запустить EasyDeskServer

Учетная запись администратора:
login: admin
password: 123
Сотрудника поддержки:
login: support
password: 123
Пользователя:
login: user
password: 123