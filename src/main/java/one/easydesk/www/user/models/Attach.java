package one.easydesk.www.user.models;

import lombok.Getter;
import lombok.Setter;
import one.easydesk.www.authentication.models.User;

import javax.persistence.*;

/**
 * Сущность файла, прикрепленного к обращению
 */
@Getter
@Setter
@Entity
public class Attach {
    @Id
    @GeneratedValue
    Long id;
    private String name;
    private String type;
    @Lob
    private byte[] data;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    public Attach() {
    }

    public Attach(String name, String type, byte[] data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }
}