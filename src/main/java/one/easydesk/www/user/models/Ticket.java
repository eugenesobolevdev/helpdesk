package one.easydesk.www.user.models;

import lombok.Getter;
import lombok.Setter;
import one.easydesk.www.admin.models.Category;
import one.easydesk.www.admin.models.Team;
import one.easydesk.www.authentication.models.User;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Сущность, представляющая собой тикет (обращение) в поддержку.
 */
@Getter
@Setter
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    private Boolean active;

    @ManyToOne
    private User applicant;

    @ManyToOne
    private User expert;

    @ManyToOne
    private Status status;

    @ManyToOne
    private Team currentTeam;

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Attach> attaches = new java.util.ArrayList<>();

    @Size(min = 1, max = 30, message = "Необходимо заполнить поле \"Краткое описание\" (не более 30 символов).")
    private String shortDescription;

    @Size(min = 3, max = 50000, message = "Опишите проблему более подробно в поле ниже")
    private String description;

    private String log;

    private String chat;

    @Transient
    private String messageToChat;

    public Ticket(){
        this.creationDate = new Date();
        this.chat = "";
        this.log = "";
        this.status = Status.REGISTERED;
        this.active = true;
    }

    public Ticket(User user){
        this.creationDate = new Date();
        this.applicant = user;
        this.chat = "";
        this.log = "";
        this.status = Status.REGISTERED;
        this.active = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id.equals(ticket.id) && Objects.equals(creationDate, ticket.creationDate) && Objects.equals(active, ticket.active) && Objects.equals(applicant, ticket.applicant) && Objects.equals(category, ticket.category) && Objects.equals(shortDescription, ticket.shortDescription) && Objects.equals(description, ticket.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creationDate, active, applicant, category);
    }
}
