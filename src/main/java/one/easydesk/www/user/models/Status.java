package one.easydesk.www.user.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Objects;


/**
 * Сущность, представляющая собой статусы обращений в поддержку.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "statuses")
public class Status {
    @Transient
    public static final Status REGISTERED = new Status(1L, "REGISTERED", "Зарегистрировано");
    @Transient
    public static final Status DECLINED = new Status(2L, "DECLINED", "Отклонено");
    @Transient
    public static final Status PROGRESS = new Status(3L, "PROGRESS", "В работе");
    @Transient
    public static final Status DONE = new Status(4L, "DONE", "Выполнено");
    @Transient
    public static final Status TRANSFERRED = new Status(5L, "TRANSFERRED", "Передано во внешнюю систему");
    @Transient
    public static final Status ARCHIVED = new Status(6L, "ARCHIVED", "В архиве");
    @Transient
    public static final Status DELETED = new Status(7L, "DELETED", "Удалено");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public Status(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Status status = (Status) o;
        return id.equals(status.id) && name.equals(status.name) && description.equals(status.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
