package one.easydesk.www.user.repositories;

import one.easydesk.www.user.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Интерфейс репозитория для работы с БД со статусами обращений
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
}
