package one.easydesk.www.user.repositories;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс репозитория для работы с обращениями
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    List<Ticket> findTicketsByApplicantAndStatusIsNotAndStatusIsNotOrderByIdDesc(User user, Status status, Status status2);
    List<Ticket> findTicketsByApplicantAndStatusIsOrderByIdDesc(User user, Status status);
    List<Ticket> findTicketsByStatusIsAndCurrentTeamIsOrderByIdDesc(Status status, Team team);
    List<Ticket> findTicketsByExpertIsAndActiveTrueOrderByIdDesc(User expert);
    Ticket findTicketByApplicantAndAndId(User user, Long id);
}
