package one.easydesk.www.user.repositories;

import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.models.Attach;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

/**
 * Интерфейс репозитория для работы с прикрепленными к обращениям файлами
 */
@Repository
public interface AttachRepository extends JpaRepository<Attach, Long> {
    Stream<Attach> findAttachesByTicket(Ticket ticket);
}
