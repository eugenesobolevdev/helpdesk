package one.easydesk.www.user.controllers;

import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Контроллер страницы с данными текущего пользователя
 */
@Controller
public class AccountController {
    private final UserService userService;

    @Autowired
    public AccountController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Get-запрос страницы для работы с данными текущего пользователя
     */
    @GetMapping("/user/account")
    public String showUserInfo(Model model, @AuthenticationPrincipal User currentUser){

        model.addAttribute("editedUser", userService.getUserById(currentUser.getId()));
        return "user/account";
    }

    @PostMapping("user/account")
    public String newTicket(Model model, @ModelAttribute(value = "editedUser") @Valid User editedUser,
                            BindingResult bindingResult, @AuthenticationPrincipal User currentUser){

        if (bindingResult.hasErrors()) {
            model.addAttribute("editUserErrors", bindingResult.getFieldErrors());
            return "user/account";
        }
        User userFromDB = userService.getUserById(editedUser.getId());
        userFromDB.setFio(editedUser.getFio());
        userFromDB.setEmail(editedUser.getEmail());
        userFromDB.setDepartment(editedUser.getDepartment());
        userFromDB.setRoles(currentUser.getRoles());
        userService.editUser(userFromDB);
        return "redirect:tickets";
    }
}
