package one.easydesk.www.user.controllers;

import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.models.Attach;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.AttachService;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Контроллер для прикрепления файлов к обращениям и их отображения
 */
@Controller
@CrossOrigin("http://localhost:8081")
class AttachController {
    private final AttachService attachService;
    private final TicketService ticketService;

    @Autowired
    public AttachController(AttachService attachService, TicketService ticketService) {
        this.attachService = attachService;
        this.ticketService = ticketService;
    }

    /**
     * Извлекает файл из БД и предоставляет его пользователю, если он является его владельцем.
     * @param id ID файла.
     * @param currentUser текущий авторизованный пользователь.
     * @return
     */
    @GetMapping("/user/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable Long id, @AuthenticationPrincipal User currentUser) {
        Attach attach = attachService.getFile(id);
        if (attach.getUser().equals(currentUser)) {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attach.getName() + "\"")
                    .body(attach.getData());
        }
        return null;
    }

    /**
     * Загружает файл в БД.
     * @param currentTicket Целевое обращение.
     * @param currentUser Целевой пользователь.
     * @param file Файл.
     */
    @PostMapping("user/upload")
    public String uploadAttach(Model model, @ModelAttribute(value = "currentTicket") Ticket currentTicket,
                            @AuthenticationPrincipal User currentUser,
                            @RequestPart("file") MultipartFile file){
            Ticket editedTicket = ticketService.getByIdOrNew(currentTicket.getId());
            try {
                if (!file.isEmpty()) attachService.store(file, editedTicket, currentUser);
                currentTicket = ticketService.getByIdOrNew(currentTicket.getId());
                ticketService.writeToLog(currentTicket, currentUser, "attach+: " + file.getOriginalFilename());
            } catch (Exception e) {
            }
        return "redirect:tickets?info=" + currentTicket.getId();
    }
}
