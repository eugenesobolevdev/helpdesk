package one.easydesk.www.user.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.admin.services.CatService;
import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.AttachService;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Date;

/**
 * Контроллер страницы со списком текущих обращений для авторизованного пользователя для роли USER
 */
@Controller
public class TicketsController {
    private final TicketService ticketService;
    private final CatService catService;
    private final AttachService attachService;

    @Autowired
    public TicketsController(TicketService ticketService, CatService catService, AttachService attachService) {
        this.ticketService = ticketService;
        this.catService = catService;
        this.attachService = attachService;
    }

    /**
     * Get-запрос на страницу для работы с обращениями текущего пользователя
     * @param infoId ID обращения, по которому необходимо вывести расширенную информацию.
     * @param archiveId ID обращения, которое необходимо перевести в архив.
     * @param add если add = 1, то отобразить форму создания нового обращения.
     * @param currentUser - текущий авторизованный пользователь.
     */
    @GetMapping("/user/tickets")
    public String showMyTickets(Model model, @RequestParam(value = "info", defaultValue = "0") Long infoId,
                                 @RequestParam(value = "archive", defaultValue = "0") Long archiveId,
                                 @RequestParam(value = "add", defaultValue = "0") int add,
                                 @AuthenticationPrincipal User currentUser){

        model.addAttribute("myTickets", ticketService.getNonArchivedTicketsByUser(currentUser));
        model.addAttribute("newTicket", ticketService.getByIdOrNew(0L));
        model.addAttribute("supportCategories", catService.getAll());
        model.addAttribute("showNewTicketForm", false);
        model.addAttribute("currentTicket", ticketService.getByUserAndId(currentUser, infoId));

        if (archiveId > 0){
            ticketService.archiveTicket(currentUser, archiveId);
            return "redirect:tickets";
        }
        if (add == 1){
            model.addAttribute("showNewTicketForm", true);
        }

        if (infoId > 0){
            model.addAttribute("attachesList", attachService.getAttachesByTicket(ticketService.getByIdOrNew(infoId), false));
        }

        return "user/tickets";

    }

    /**
     * POST-запрос на создание нового обращения.
     * @param model Контейнер данных.
     * @param newTicket Сущность нового обращения.
     * @param bindingResult Результат валидации формы.
     * @param currentTicket  Редактируемое обращение.
     * @param currentUser Текущий авторизованный пользователь.
     * @param file Прикрепляемый файл.
     */
    @PostMapping("user/tickets")
    public String newTicket(Model model, @ModelAttribute(value = "newTicket") @Valid Ticket newTicket,
                            BindingResult bindingResult,
                            @ModelAttribute(value = "currentTicket") Ticket currentTicket,
                            @AuthenticationPrincipal User currentUser,
                            @RequestPart("file") MultipartFile file){

            if (bindingResult.hasErrors()) {
                model.addAttribute("myTickets", ticketService.getNonArchivedTicketsByUser(currentUser));
                model.addAttribute("supportCategories", catService.getAll());
                model.addAttribute("showNewTicketForm", true);
                model.addAttribute("showArchiveConfirmForm", false);
                model.addAttribute("newTicketFormErrors", bindingResult.getFieldErrors());
                model.addAttribute("currentTicket", ticketService.getByUserAndId(currentUser, 0L));
                model.addAttribute("currentTicketForMessage", ticketService.getByUserAndId(currentUser, 0L));
                return "user/tickets";
            }
            newTicket.setCurrentTeam(catService.getDefaultTeamById(newTicket.getCategory().getId()));
            newTicket.setApplicant(currentUser);
            newTicket.setCreationDate(new Date());
            newTicket.setStatus(Status.REGISTERED);
            ticketService.writeToLog(newTicket, currentUser, "created.");
            ticketService.writeToLog(newTicket, currentUser, "auto-assigned team: \"" +
                    newTicket.getCurrentTeam().getName() + "\"");
            ticketService.messageToChat(newTicket, "Ваше обращение зарегистрировано!\n");
            ticketService.save(newTicket);
            try {
                if (!file.isEmpty()) attachService.store(file, newTicket, currentUser);
                EasyDeskServer.LOGGER.info("Uploaded the file successfully: {}",
                        file.getOriginalFilename());
            } catch (Exception e) {
                EasyDeskServer.LOGGER.error("Could not upload the file: {}!",
                        file.getOriginalFilename());
            }

        return "redirect:tickets";
    }
}
