package one.easydesk.www.user.controllers;

import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Контроллер POST - запроса на смену пароля текущего пользователя со страницы пользователя
 */
@Controller
public class PasswordController {
    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public PasswordController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Обрабатывает POST - запрос на смену пароля текущего пользователя.
     * Из CurrentUser получаем ID текущего пользователя. По данному ID извлекаем запись из БД в editedUser.
     * При успешном прохождении тестов на правильность паролей переносим их из userEntity в editedUser.
     * Сохраняем editedUser в БД.
     * @param userEntity сущность, полученная из фронта. Является урезанной. Содержит в себе только введенные пользователем
     *                   данные: старый пароль, новый и его подстверждение.
     * @param currentUser сущность текущего пользователя, авторизованного в Spring Security.
     */
    @PostMapping("user/password")
    public String newTicket(HttpServletRequest request, RedirectAttributes redirectAttributes, @ModelAttribute(value = "editedUser") User userEntity,
                            @AuthenticationPrincipal User currentUser){

        if (!bCryptPasswordEncoder.matches(userEntity.getOldPassword(), currentUser.getPassword())) {
            redirectAttributes.addFlashAttribute("passwordError", "Старый пароль введен неверно!");
            return "redirect:account";
        }

        if (!userEntity.getNewPassword().equals(userEntity.getPasswordConfirm())) {
            redirectAttributes.addFlashAttribute("passwordError", "Введенные пароли не совпадают!");
            return "redirect:account";
        }

        User editedUser = userService.getUserById(currentUser.getId());
        editedUser.setNewPassword(userEntity.getNewPassword());
        userService.editUser(editedUser);
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return "redirect:tickets";
    }
}
