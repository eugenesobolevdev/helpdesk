package one.easydesk.www.user.controllers;

import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Контроллер переписки пользователя внутри обращения
 */
@Controller
public class ChatController {
    private final TicketService ticketService;

    @Autowired
    public ChatController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * POST - запрос на добавление сообщения в обращение.
     @param currentTicket Целевое обращение, полученное из фронта.
     */
    @PostMapping("user/chat")
    public String addMessageToChat(Model model, @ModelAttribute(value = "currentTicket") Ticket currentTicket){
            Ticket ticketFromDB = ticketService.getByIdOrNew(currentTicket.getId());
            if (!currentTicket.getMessageToChat().isEmpty()){
                ticketService.messageToChat(ticketFromDB, ">>" + currentTicket.getMessageToChat());
            }
        return "redirect:tickets?info=" + currentTicket.getId();
    }
}
