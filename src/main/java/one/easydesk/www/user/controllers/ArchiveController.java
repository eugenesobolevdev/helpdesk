package one.easydesk.www.user.controllers;

import one.easydesk.www.admin.services.CatService;
import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.services.AttachService;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Контроллер страницы со списком архивных обращений роли USER
 */
@Controller
public class ArchiveController {
    private final TicketService ticketService;
    private final AttachService attachService;

    @Autowired
    public ArchiveController(TicketService ticketService, CatService catService, TeamService teamService, AttachService attachService) {
        this.ticketService = ticketService;
        this.attachService = attachService;
    }

    /**
     * Get-запрос на страницу для работы с архивными обращениями текущего пользователя
     * @param infoId ID обращения, по которому необходимо показать расширенную информацию
     * @param deleteId ID обращения, статус которого необходимо изменить на "DELETED"
     * @param currentUser текущий авторизованный пользователь
     * @return
     */
    @GetMapping("/user/archive")
    public String showMyArchivedTickets(Model model, @RequestParam(value = "info", defaultValue = "0") Long infoId,
                                 @RequestParam(value = "delete", defaultValue = "0") Long deleteId,
                                 @AuthenticationPrincipal User currentUser){

        model.addAttribute("myTickets", ticketService.getArchivedTicketsByUser(currentUser));
        model.addAttribute("currentTicket", ticketService.getByUserAndId(currentUser, infoId));

        if (infoId > 0){
            model.addAttribute("attachesList", attachService.getAttachesByTicket(ticketService.getByIdOrNew(infoId), false));
        }

        if (deleteId > 0){
            ticketService.deleteTicket(currentUser, deleteId);
            return "redirect:archive";
        }

        return "user/archive";

    }
}
