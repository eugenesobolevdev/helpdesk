package one.easydesk.www.user.services;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Сервис, реализующий связь между UI и БД для обращений
 */
@Service
public class TicketService {
    private final TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    /**
     * @return список всех обращений из БД
     */
    public Collection<Ticket> getAll(){
        return this.ticketRepository.findAll();
    }

    /**
     * @param user Пользователь
     * @return Список всех неархивных и неудаленных обращений заданного пользователя.
     */
    public Collection<Ticket> getNonArchivedTicketsByUser(User user){
        return ticketRepository.findTicketsByApplicantAndStatusIsNotAndStatusIsNotOrderByIdDesc(user, Status.ARCHIVED, Status.DELETED);
    }

    /**
     * @param user Пользователь
     * @return Список всех архивных обращений заданного пользователя.
     */
    public Collection<Ticket> getArchivedTicketsByUser(User user){
        return ticketRepository.findTicketsByApplicantAndStatusIsOrderByIdDesc(user, Status.ARCHIVED);
    }

    /**
     * Перемещение обращения в архив.
     * @param user Пользователь.
     * @param id ID обращения.
     * @return Результат операции.
     */
    public boolean archiveTicket(User user, Long id){
        Ticket ticket = ticketRepository.findTicketByApplicantAndAndId(user, id);
        if (ticket == null) return false;
        writeToLog(ticket, user, "Moved to the archive.");
        messageToChat(ticket, "Вы перевели обращение в архив.");
        ticket.setStatus(Status.ARCHIVED);
        save(ticket);
        return true;
    }

    /**
     * Перемещение обращения в список удаленных обращений.
     * @param user Пользователь.
     * @param id ID обращения.
     * @return Результат операции.
     */
    public boolean deleteTicket(User user, Long id){
        Ticket ticket = ticketRepository.findTicketByApplicantAndAndId(user, id);
        if (ticket == null) return false;
        writeToLog(ticket, user, "Deleted.");
        ticket.setStatus(Status.DELETED);
        save(ticket);
        return true;
    }

    /**
     * Удаляет обращение из БД по ID
     * @param id ID
     */
    public void deleteTicketFromDB(Long id){
        if (ticketRepository.existsById(id))
            ticketRepository.deleteById(id);
    }

    /**
     * Возвращает обращение по его ID, если его пользователь совпадает с заданным.
     * @param user Пользователь.
     * @param id ID обращения.
     * @return Обращение с заданным ID и пользователем.
     */
    public Ticket getByUserAndId(User user, Long id){
        Ticket result = ticketRepository.findTicketByApplicantAndAndId(user, id);
        if (result == null) result = new Ticket(user);
        return result;
    }

    /**
     * Возвращает обращение из БД по его айди, в случае отсутствия, создает новое обращение
     * от имени текущего пользователя.
     * @param id ID.
     * @return Запись из БД по её ID.
     */
    public Ticket getByIdOrNew(Long id){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ticketRepository.findById(id).orElse(new Ticket(user));
    }

    /**
     * @param id ID
     * @return Обращение по ID
     */
    public Ticket getById(Long id){
        return ticketRepository.findById(id).orElse(null);
    }

    /**
     * Добавляет сообщение в чат внутри обращения.
     * @param ticket Заданное обращение.
     * @param message Сообщение.
     */
    public void messageToChat(Ticket ticket, String message){
        String chat = ticket.getChat();
        chat = new Date().toLocaleString() + " " + message + "\n" + chat;
        ticket.setChat(chat);
        save(ticket);
    }

    /**
     * Добавляет запись в историю действия с обращением.
     * @param ticket Заданное обращение.
     * @param user Пользователь, который произвел действие.
     * @param logRecord Запись
     */
    public void writeToLog(Ticket ticket, User user, String logRecord){
        String log = ticket.getLog();
        log = new Date().toLocaleString() + " *" + user.getUsername()
                + "* " + logRecord + "\n" + log;
        ticket.setLog(log);
        save(ticket);
    }

    /**
     * Удаляет запись в БД по её ID
     * @param id
     */
    public void deleteById(Long id){
        if (ticketRepository.existsById(id)) {
            ticketRepository.deleteById(id);
        }
    }

    /**
     * Обновляет запись в БД или создает новую
     */
    public void save(Ticket ticket){
        ticketRepository.save(ticket);
    }

    /**
     * @param status Статус обращения
     * @param team Группа поддержки
     * @return Список обращений заданной группы в заданном статусе.
     */
    public Collection<Ticket> getAllTicketsByStatusAndTeamAndActivity (Status status, Team team){
        return ticketRepository.findTicketsByStatusIsAndCurrentTeamIsOrderByIdDesc(status, team);
    }

    public Collection<Ticket> getAllActiveTicketsByExpert(User expert){
        return ticketRepository.findTicketsByExpertIsAndActiveTrueOrderByIdDesc(expert);
    }
}
