package one.easydesk.www.user.services;

import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Сервис, реализующий связь между UI и БД для статусов
 */
@Service
public class StatusService {
    private final StatusRepository statusRepository;

    @Autowired
    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    /**
     * @return список всех статусов из БД
     */
    public List<Status> getAll()
    {
        return statusRepository.findAll();
    }

    /**
     * @return статус из БД по ID
     */
    public Status getById(Long id)
    {
        return statusRepository.findById(id).orElse(Status.REGISTERED);
    }
}
