package one.easydesk.www.user.services;

import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.message.ResponseFile;
import one.easydesk.www.user.models.Attach;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.repositories.AttachRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Сервис, реализующий связь между UI и БД для файлов, прикрепленных к обращениям
 */
@Service
public class AttachService {
    private final AttachRepository attachRepository;

    @Autowired
    public AttachService(AttachRepository attachRepository) {
        this.attachRepository = attachRepository;
    }

    /**
     * Сохраняет файл в БД
     * @param file Файл
     * @param ticket Целевое обращение
     * @param user Владелец файла
     */
    public void store(@NotNull MultipartFile file, Ticket ticket, User user) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Attach attach = new Attach(fileName, file.getContentType(), file.getBytes());
        attach.setTicket(ticket);
        attachRepository.save(attach);
        attach.setUser(user);
        attachRepository.save(attach);
    }

    /**
     * Получает файл из БД по его ID
     * @param id ID
     * @return файл
     */
    public Attach getFile(Long id) {
        return attachRepository.findById(id).get();
    }

    /**
     * @return Стрим всех файлов
     */
    public Stream<Attach> getAllFiles() {
        return attachRepository.findAll().stream();
    }

    /**
     * @param ticket Обращение
     * @return Список всех файлов из заданного обращения
     */
    @Transactional
    public List<ResponseFile> getAttachesByTicket(Ticket ticket, Boolean isItForSupport) {
        return attachRepository.findAttachesByTicket(ticket).map(dbFile -> {
            String fileDownloadUri;
            if (isItForSupport) {
                fileDownloadUri = ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path("/support/files/")
                        .path(String.valueOf(dbFile.getId()))
                        .toUriString();
            } else {
                fileDownloadUri = ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path("/user/files/")
                        .path(String.valueOf(dbFile.getId()))
                        .toUriString();
            }
            return new ResponseFile(
                    dbFile.getName(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
    }
}
