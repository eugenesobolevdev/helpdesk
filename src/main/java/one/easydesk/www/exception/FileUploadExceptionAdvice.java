package one.easydesk.www.exception;
import one.easydesk.www.EasyDeskServer;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Перехватчик ошибок при добавлении файлов в обращение.
 * Добавляет в аттрибуты код ошибки и возвращает на адрес,
 * который зависит от того, откуда пришел запрос.
 */
@ControllerAdvice
public class FileUploadExceptionAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleMaxSizeException(MaxUploadSizeExceededException exception,
                                         RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
        String request = httpServletRequest.getHeader("referer");
        String redirectAddress = "";
        if (request.contains("/user/")) {
            if (request.contains("add=1")) {
                redirectAddress = "redirect:/user/tickets?add=1";
            }
            if (request.contains("info=")){
                String id = request.substring(request.lastIndexOf("info=") + 5);
                redirectAddress = "redirect:/user/tickets?info=" + id;
            }
        }
        if (request.contains("/support/")) {
            String id = request.substring(request.lastIndexOf("id=") + 3);
            redirectAddress = "redirect:/support/ticket?id=" + id;
        }
        redirectAttributes.addFlashAttribute("attachError", "Файл слишком большой");
        EasyDeskServer.LOGGER.warn("attach from {} is bigger than 5mb", redirectAddress);
        return redirectAddress;
    }
}