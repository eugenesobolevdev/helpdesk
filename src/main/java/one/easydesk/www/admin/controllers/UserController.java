package one.easydesk.www.admin.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import java.util.*;

/**
 * Контроллер для работы с учетными записями и правами пользователей
 * MAIN_ADMIN_ID - ID учетной записи главного администратора.
 * Запрещено ее удаление и снятие админской роли
 */
@Controller
public class UserController {
    final int MAIN_ADMIN_ID = 1;
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * отображает список пользователей, их права и позволяет их редактировать.
     * @param model модель данных из фронта
     * @param editId ID редактируемого пользователя
     * @param deleteId ID записи, которую необходимо удалить
     * @param add если данный параметр больше 0, до добавляем новую запись
     * @return
     */
    @GetMapping("/admin/users")
    public String getUsers(Model model,
                                  @RequestParam(value = "edit", defaultValue = "0") Long editId,
                                  @RequestParam(value = "delete", defaultValue = "0") Long deleteId,
                                  @RequestParam(value = "add", defaultValue = "0") int add){
        model.addAttribute("allUsers", userService.getAll());
        User editedUser = userService.getUserById(editId);
        model.addAttribute("editedUser", editedUser);
        model.addAttribute("isEditedUserSupport", editedUser.containRoleSupport());
        model.addAttribute("isEditedUserAdmin", editedUser.containRoleAdmin());
        model.addAttribute("newUser", new User());
        if (deleteId > 0 && deleteId != MAIN_ADMIN_ID){
            userService.deleteUser(deleteId);
            return "redirect:users";
        }
        if (add > 0){
            userService.addUser(new User());
            return "redirect:users";
        }
        EasyDeskServer.LOGGER.info("Get 'admin/users'");
        return "admin/users";
    }

    /**
     * Получает отредактированную или новую сущность пользователя.
     * @param model модель данных из фронта
     * @param editedUser Сущность редактируемого пользователя, полученная из UI
     * @param newUser сущность нового пользователя (созданного на странице)
     * @param form на странице во фронте две формы для ввода данных, возможны два POST - запроса, в зависимости
     *             от того, кнопка какой формы нажата:
     *        режим form=1 используется для редактирования пользователя внутри списка пользователей.
     *        режим form=2 используется для добавления в БД нового пользователя (созданного на странице)
     * @return html-страница
     */
    @PostMapping("/admin/users")
    public String postUser(Model model, @ModelAttribute(value = "editedUser") @Valid User editedUser, BindingResult bindingResult,
                           @ModelAttribute(value = "newUser") User newUser,
                           @RequestParam(value = "form", defaultValue = "0") Long form){
        if (form == 1) {
            if (bindingResult.hasErrors()){
                model.addAttribute("allUsers", userService.getAll());
                model.addAttribute("newUser", new User());
                model.addAttribute("editUserErrors", bindingResult.getFieldErrors());
                return "admin/users";
            }
            Set<Role> roles = new HashSet<>();
            roles.add(Role.ROLE_USER);
            if (editedUser.isSupport()) roles.add(Role.ROLE_SUPPORT);
            if (editedUser.isAdmin()) roles.add(Role.ROLE_ADMIN);
            editedUser.setRoles(roles);
            editedUser.setAttach(userService.getUserById(editedUser.getId()).getAttach());
            userService.editUser(editedUser);
            return "redirect:users";
        }
        if (form == 2) {
            HashSet<Role> roles = new HashSet<>();
            roles.add(Role.ROLE_USER);
            model.addAttribute("allUsers", userService.getAll());
            if (newUser.getUsername().equals("")){
                model.addAttribute("newUsernameError", "Логин не может быть пустым!");
                return "admin/users";
            }
            if (!userService.addUser(new User(newUser.getUsername(), newUser.getPassword(), roles)))
            {
                model.addAttribute("newUsernameError", "Такой логин уже существует!");
                return "admin/users";
            }
            if (newUser.getPassword().equals("")){
                model.addAttribute("newPasswordError", "Пароль не может быть пустым!");
                return "admin/users";
            }
        }
        EasyDeskServer.LOGGER.info("Post 'admin/users'");
        return "redirect:users";
    }
}
