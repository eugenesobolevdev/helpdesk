package one.easydesk.www.admin.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.admin.models.Category;
import one.easydesk.www.admin.services.CatService;
import one.easydesk.www.admin.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Контроллер админской части. Обрабатывает запросы клиента
 */
@Controller
public class CatsController {
    private final CatService catService;
    private final TeamService teamService;

    @Autowired
    public CatsController(CatService catService, TeamService teamService) {
        this.catService = catService;
        this.teamService = teamService;
    }

    /**
     * Get-запрос на страницу для работы с категориями поддержки
     * @param model контейнер данных
     * @param editId ID записи для редактирования (если необходимо)
     * @param deleteId ID записи для удаления (если необходимо)
     * @param add = 1 если необходимо создать новую запись
     * @return
     */
    @GetMapping("/admin/cats")
    public String showSupportCat(Model model, @RequestParam(value = "edit", defaultValue = "0") Long editId,
                                 @RequestParam(value = "delete", defaultValue = "0") Long deleteId,
                                 @RequestParam(value = "add", defaultValue = "0") int add){

        model.addAttribute("supportCategories", catService.getAll());
        model.addAttribute("supportGroups", teamService.getAll());
        model.addAttribute("editedSupportCategory", catService.getById(editId));

        if (deleteId > 0){
            catService.deleteById(deleteId);
            return "redirect:cats";
        }
        if (add == 1){
            Category category = new Category("Категория №" + (catService.getCount() + 1), "Описание", teamService.getById(1L));
            catService.edit(category);
            return "redirect:cats?edit=" + category.getId();
        }
        EasyDeskServer.LOGGER.info("Get 'admin/cats'");
        return "admin/cats";

    }

    /**
     * POST-запрос на страницу для работы с категориями поддержки.
     * Обновляет запись или добавляет новую.
     * @param model контейнер данных
     * @param category категория для добавления/обновления
     * @return
     */
    @PostMapping("admin/cats")
    public String editCat(Model model, @ModelAttribute(value = "editedSupportCategory") Category category){
        catService.edit(category);
        EasyDeskServer.LOGGER.info("Post 'admin/cats'");
        return "redirect:cats";
    }
}
