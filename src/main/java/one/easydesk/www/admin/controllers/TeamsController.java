package one.easydesk.www.admin.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.admin.models.Team;
import one.easydesk.www.admin.services.CatService;
import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Контроллер для работы с командами поддержки.
 * MAIN_TEAM_ID - ID учетной записи первой линии поддержки. Запрещено удаление этой группы
 */
@Controller
public class TeamsController {
    final int MAIN_TEAM_ID = 1;
    private final TeamService teamService;
    private final UserService userService;

    @Autowired
    public TeamsController(TeamService teamService, UserService userService) {
        this.teamService = teamService;
        this.userService = userService;
    }

    /**
     * Отображает список команд, позволяет добавлять их, удалять, редактировать название и описание
     * @param model модель данных из фронта
     * @param editId ID редактируемой группы
     * @param deleteId ID удаляемой группы
     * @param add если данный параметр больше 0, до добавляем новую группу
     * @return html-страница
     */
    @GetMapping("/admin/teams")
    public String showSupportGroups(Model model, @RequestParam(value = "edit", defaultValue = "0") long editId,
                                    @RequestParam(value = "delete", defaultValue = "0") long deleteId,
                                    @RequestParam(value = "add", defaultValue = "0") int add){

        model.addAttribute("teams", teamService.getAll());
        model.addAttribute("editedTeam", teamService.getById(editId));

        if (deleteId > 0 && deleteId != MAIN_TEAM_ID){
            teamService.deleteById(deleteId);
            return "redirect:teams";
        }
        if (add > 0){
            Team team = new Team("Группа №" + (teamService.getCount() + 1), "Описание");
            teamService.edit(team);
            return "redirect:teams?edit=" + team.getId();
        }
        EasyDeskServer.LOGGER.info("Get 'admin/teams'");
        return "admin/teams";

    }

    /**
     * получает отредактированную сущность группы.
     * @param model модель данных из фронта
     * @param supportTeam сущность редактирумой группы
     * @return html-страница
     */
    @PostMapping("/admin/teams")
    public String editTeam(Model model, @ModelAttribute(value = "editedTeam") Team supportTeam){
        teamService.edit(supportTeam);
        return "redirect:teams";
    }

    /**
     * Отображает и позволяет редактировать список сотрудников определенной группы.
     * @param model модель данных фронта
     * @param id ID редактируемой группы
     * @param deleteId ID пользователя, который удаляется из группы
     * @param add ID пользователя, лобавляемого в группу
     * @return html-страница
     */
    @GetMapping("/admin/teamedit")
    public String showTeamExperts(Model model, @RequestParam(value = "id", defaultValue = "0") Long id,
                                  @RequestParam(value = "delete", defaultValue = "0") Long deleteId,
                                  @RequestParam(value = "add", defaultValue = "0") Long add){
        Team editedTeam = teamService.getById(id);
        model.addAttribute("allTeams", teamService.getAll());
        model.addAttribute("editedTeam", editedTeam);
        model.addAttribute("availableExperts", userService.showAllByRoleAndTeam(Role.ROLE_SUPPORT, editedTeam));

        if (deleteId > 0 && id > 0){
            User user = userService.getUserById(deleteId);
            user.getTeams().remove(editedTeam);
            userService.editUser(user);
            return "redirect:teamedit?id="+id;
        }
        if (add > 0 && id > 0){
            User user = userService.getUserById(add);
            user.getTeams().add(editedTeam);
            userService.editUser(user);
            return "redirect:teamedit?id=" + id;
        }
        EasyDeskServer.LOGGER.info("Post 'admin/teams'");
        return "admin/teamedit";
    }
}
