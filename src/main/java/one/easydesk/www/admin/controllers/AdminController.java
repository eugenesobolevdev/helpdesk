package one.easydesk.www.admin.controllers;

import one.easydesk.www.EasyDeskServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Котроллер админской части
 */
@Controller
@RequestMapping("/")
public class AdminController {
    @GetMapping("admin/")
    public String adminMapping(){
        EasyDeskServer.LOGGER.info("Redirected to 'admin/cats' from 'admin/'");
        return "redirect:cats";
    }
}
