package one.easydesk.www.admin.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import one.easydesk.www.authentication.models.User;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * Сущность, представляющая собой элемент группы поддержки.
 * Имеет связь Many-to-many с сущностью User - учетной записи пользователя
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "teams")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "teams")
    private Set<User> users = new java.util.LinkedHashSet<>();

    public Team(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return id.equals(team.id) && Objects.equals(name, team.name) && Objects.equals(description, team.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}