package one.easydesk.www.admin.repositories;

import one.easydesk.www.admin.models.Category;
import one.easydesk.www.admin.models.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Интерфейс репозитория для работы с БД с категориями поддержки
 */
@Repository
public interface CatRepository extends JpaRepository<Category, Long> {
}
