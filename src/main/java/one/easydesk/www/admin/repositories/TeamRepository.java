package one.easydesk.www.admin.repositories;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.authentication.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс репозитория для работы с БД с группами поддержки
 */
@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    List<Team> findTeamsByUsersOrderById(User user);
    List<Team> findTeamsByIdIsNot(Long id);
}
