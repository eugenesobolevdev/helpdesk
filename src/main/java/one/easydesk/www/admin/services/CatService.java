package one.easydesk.www.admin.services;

import one.easydesk.www.admin.models.Category;
import one.easydesk.www.admin.models.Team;
import one.easydesk.www.admin.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Сервис, реализующий связь между UI и БД для категорий поддержки
 */
@Service
public class CatService {
    private final CatRepository catRepository;

    @Autowired
    public CatService(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    /**
     * @return список всех категорий из БД
     */
    public Collection<Category> getAll(){
        return this.catRepository.findAll();
    }

    /**
     * @param id
     * @return запись из БД по её ID
     */
    public Category getById(Long id){
        return catRepository.findById(id).orElse(new Category());
    }

    /**
     * Удаляет запись в БД по её ID
     * @param id
     */
    public void deleteById(Long id){
        if (catRepository.existsById(id)) {
            catRepository.deleteById(id);
        }
    }

    /**
     * Обновляет запись в БД или создает новую
     * @param category
     */
    public void edit(Category category){
        catRepository.save(category);
    }

    /**
     * @return количество категорий в БД
     */
    public long getCount(){
        return catRepository.count();
    }

    public Team getDefaultTeamById(Long id){
        Optional<Category> category = catRepository.findById(id);
        return category.get().getTeam();
    }
}
