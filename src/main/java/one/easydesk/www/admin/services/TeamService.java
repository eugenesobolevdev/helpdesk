package one.easydesk.www.admin.services;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.admin.repositories.TeamRepository;
import one.easydesk.www.authentication.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Сервис, реализующий связь между UI и БД для групп поддержки
 */
@Service
public class TeamService {
    private final TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }


    /**
     * @return список всех групп из БД
     */
    public ArrayList<Team> getAll(){
        return (ArrayList<Team>) teamRepository.findAll();
    }

    /**
     * @return команду из БД по её ID
     */
    public Team getById(Long id)
    {
        return teamRepository.findById(id).orElse(new Team());
    }

    /**
     * Удаляет команду в БД по её ID
     */
    public void deleteById(Long id){
        if (teamRepository.existsById(id)) {
            teamRepository.deleteById(id);
        }
    }

    /**
     * Обновляет команду в БД или создает новую
     */
    public void edit(Team supportGroup){
        teamRepository.save(supportGroup);
    }

    /**
     * @return количество групп в БД
     */
    public long getCount(){
        return teamRepository.count();
    }

    /**
     * @param user Пользователь
     * @return Отсортированный список групп, в которых состоит пользователь.
     */
    public List<Team> getSortedTeamsByUser(User user){
        return teamRepository.findTeamsByUsersOrderById(user);
    }

    /**
     * @param exceptId Заданная группа
     * @return Список всех групп, кроме заданной.
     */
    public List<Team> getAllTeamsExceptOne(Long exceptId){
        return teamRepository.findTeamsByIdIsNot(exceptId);
    }
}
