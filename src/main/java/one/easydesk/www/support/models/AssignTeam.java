package one.easydesk.www.support.models;

/**
 * Вспомогательный класс, предназначенный для хранения данных о команде, на которую произодится
 * переназначение, а также о предыдущей команде.
 */
public class AssignTeam {
    private Long newTeamId;
    private Long ticketId;
    private String comment;

    public AssignTeam(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getNewTeamId() {
        return newTeamId;
    }

    public void setNewTeamId(Long newTeamId) {
        this.newTeamId = newTeamId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}


