package one.easydesk.www.support.models;

/**
 * Вспомогательный класс, предназначенный для хранения данных о результатах решения обращения.
 */
public class Solution {
    private String solution;
    private Long ticketId;

    public Solution(Long ticketId) {
        this.ticketId = ticketId;
        this.solution = "";
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }
}


