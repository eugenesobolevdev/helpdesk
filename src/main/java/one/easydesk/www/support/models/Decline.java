package one.easydesk.www.support.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Вспомогательный класс, предназначенный для хранения данных о причинах отклонения обращения.
 */
@Getter
@Setter
public class Decline {
    private String decline;
    private Long ticketId;

    public Decline() {
    }

    public Decline(Long ticketId) {
        this.ticketId = ticketId;
    }
}


