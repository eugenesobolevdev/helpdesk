package one.easydesk.www.support.models;

/**
 * Вспомогательный класс, предназначенный для хранения данных о специалисте, на которого
 * произврдится назначение, а также о предыдущем специалисте.
 */
public class AssignExpert {
    private Long newExpertId;
    private Long ticketId;
    private String comment;

    public AssignExpert(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getNewExpertId() {
        return newExpertId;
    }

    public void setNewExpertId(Long newExpertId) {
        this.newExpertId = newExpertId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}


