package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import one.easydesk.www.support.models.AssignExpert;
import one.easydesk.www.support.models.Decline;
import one.easydesk.www.support.models.AssignTeam;
import one.easydesk.www.support.models.Solution;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.AttachService;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Контроллер страницы для работы с определенным обращением
 */
@Controller
public class TicketController {
    private final TicketService ticketService;
    private final AttachService attachService;
    private final UserService userService;
    private final TeamService teamService;

    @Autowired
    public TicketController(TicketService ticketService, AttachService attachService, UserService userService, TeamService teamService) {
        this.ticketService = ticketService;
        this.attachService = attachService;
        this.userService = userService;
        this.teamService = teamService;
    }

    /**
     * Обработчик Get - запроса.
     * @param model - Модель данных.
     * @param ticketId - ID редактируемого обращения.
     * @param takeToWorkId - ID обращения, которое необходимо взять в работу под текущим специалистом.
     * @param stealId - ID обращения, в котором необходимо сменить специалиста на текущего авторизованного.
     * @param currentUser - Текущий авторизованный пользователь.
     * @return Страница переадресации.
     * Передаваемые в UI аттрибуты:
     * ticket - Редактируемое обращение.
     * solution - Вспомогательный класс, предназначенный для хранения данных о результатах решения обращения.
     * decline - Вспомогательный класс, предназначенный для хранения данных о причинах отклонения обращения.
     * assignTeam - Вспомогательный класс, предназначенный для хранения данных о команде, на которую произодится
     *              переназначение, а также о предыдущей команде.
     * assignExpert - Вспомогательный класс, предназначенный для хранения данных о специалисте, на которого
     *              произврдится назначение, а также о предыдущем специалисте.
     * currentUser - Текущий авторизованный пользователь.
     * availableExperts - Список специалистов, доступных для назначения.
     * availableTeams - Список групп, доступных для переназначения.
     * attachesList - Список приложенных к обращению файлов
     * readOnly - true - обращение доступно только для чтения.
     */
    @GetMapping("/support/ticket")
    public String showTicket(Model model, @RequestParam(value = "id", defaultValue = "0") Long ticketId,
                             @RequestParam(value = "taketowork", defaultValue = "0") Long takeToWorkId,
                             @RequestParam(value = "steal", defaultValue = "0") Long stealId,
                             @AuthenticationPrincipal User currentUser){

        Ticket currentTicket = ticketService.getByIdOrNew(ticketId);

        if (currentTicket.getId() == null)
        {
            model.addAttribute("ticketNumber", ticketId);
            return "support/ticketnotfound";
        }
        currentUser = userService.getUserById(currentUser.getId());
        model.addAttribute("ticket", currentTicket);
        model.addAttribute("solution", new Solution(currentTicket.getId()));
        model.addAttribute("decline", new Decline(currentTicket.getId()));
        model.addAttribute("assignTeam", new AssignTeam(currentTicket.getId()));
        model.addAttribute("assignExpert", new AssignExpert(currentTicket.getId()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("availableExperts", userService.showAllByRoleExceptOne(Role.ROLE_SUPPORT,
                currentTicket.getCurrentTeam(), currentUser));
        model.addAttribute("availableTeams", teamService.getAllTeamsExceptOne(currentTicket.getCurrentTeam().getId()));
        model.addAttribute("attachesList", attachService.getAttachesByTicket(ticketService.getByIdOrNew(ticketId), true));
        model.addAttribute("readOnly", (!currentUser.getTeams().contains(currentTicket.getCurrentTeam()) || !currentTicket.getActive()));

        if(takeToWorkId > 0){
            if(currentTicket.getStatus().equals(Status.REGISTERED)
            && currentUser.getTeams().contains(currentTicket.getCurrentTeam())){
                currentTicket.setStatus(Status.PROGRESS);
                currentTicket.setExpert(currentUser);
                ticketService.messageToChat(currentTicket, "Ваше обращение принято в работу");
                ticketService.writeToLog(currentTicket, currentUser, "taken to work.");
                ticketService.save(currentTicket);
                return "redirect:ticket?id=" + currentTicket.getId();
            }
        }

        if(stealId > 0){
            if(currentUser.getTeams().contains(currentTicket.getCurrentTeam())){
                final String oldUsername = currentTicket.getExpert() == null ? "" : currentTicket.getExpert().getUsername();
                ticketService.writeToLog(currentTicket, currentUser, "stole the ticket from \"" +
                        oldUsername + "\"");
                currentTicket.setExpert(currentUser);
                ticketService.save(currentTicket);
                return "redirect:ticket?id=" + currentTicket.getId();
            }
        }
        EasyDeskServer.LOGGER.info("Get 'support/tickets'");
        return "support/ticket";

    }
}
