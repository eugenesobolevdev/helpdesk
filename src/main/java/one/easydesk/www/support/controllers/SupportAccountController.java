package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

/**
 * Контроллер страницы с данными текущего специалиста
 */
@Controller
public class SupportAccountController {
    private final UserService userService;

    @Autowired
    public SupportAccountController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Get-запрос страницы для работы с данными текущего специалиста
     */
    @GetMapping("/support/account")
    public String showUserInfo(Model model, @AuthenticationPrincipal User currentUser){

        model.addAttribute("editedUser", userService.getUserById(currentUser.getId()));
        EasyDeskServer.LOGGER.info("Get 'support/account'");
        return "support/account";
    }

    @PostMapping("support/account")
    public String newTicket(Model model, @ModelAttribute(value = "editedUser") @Valid User editedUser,
                            BindingResult bindingResult, @AuthenticationPrincipal User currentUser){

        if (bindingResult.hasErrors()) {
            model.addAttribute("editUserErrors", bindingResult.getFieldErrors());
            return "support/account";
        }
        User userFromDB = userService.getUserById(editedUser.getId());
        userFromDB.setEmail(editedUser.getEmail());
        userFromDB.setFio(editedUser.getFio());
        userFromDB.setDepartment(editedUser.getDepartment());
        userFromDB.setRoles(currentUser.getRoles());
        userService.editUser(userFromDB);
        EasyDeskServer.LOGGER.info("Post 'support/account'");
        return "redirect:tickets?team=0&status=1";
    }
}
