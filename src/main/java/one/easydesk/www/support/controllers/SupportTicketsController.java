package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.admin.models.Team;
import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.services.StatusService;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер страницы со списком текущих обращений для авторизованного пользователя для роли SUPPORT
 */
@Controller
public class SupportTicketsController {
    private final TicketService ticketService;
    private final TeamService teamService;
    private final StatusService statusService;

    @Autowired
    public SupportTicketsController(TicketService ticketService, TeamService teamService, StatusService statusService) {
        this.ticketService = ticketService;
        this.teamService = teamService;
        this.statusService = statusService;
    }

    /**
     * Get-запрос на страницу для работы с обращениями
     * @param currentUser - текущий авторизованный пользователь.
     * Передаваемые в UI аттрибуты:
     * currentExpertSupportTeams - список групп поддержки, в которых состоит текущий пользователь.
     * selectedTeam - Выбранная через параметр адресной строки команда.
     * allSupportTeams - Список всех групп.
     * allStatuses - Список всех статусов.
     * selectedStatus - Выбранный через параметр адресной строки статус.
     * selectedTicket - Обрашение, по которому необходимо вывести расширенную информацию.
     * tickets - Список обращений, соответствующих настройке фильтра.
     */
    @GetMapping("/support/tickets")
    public String showTickets(Model model, @RequestParam(value = "ticket", defaultValue = "0") Long ticketId,
                              @RequestParam(value = "team", defaultValue = "1") Long teamId,
                              @RequestParam(value = "status", defaultValue = "1") Long statusId,
                              @AuthenticationPrincipal User currentUser){

        if (teamId == 0) {
            List<Team> teamList = new ArrayList<>();
            teamList.addAll(currentUser.getTeams());
            if (teamList.isEmpty()) teamId = 1L;
            else {
                teamId = teamList.get(0).getId();
            }
        }
        model.addAttribute("currentExpertSupportTeams", teamService.getSortedTeamsByUser(currentUser));
        model.addAttribute("selectedTeam", teamService.getById(teamId));
        model.addAttribute("allSupportTeams", teamService.getAll());
        model.addAttribute("allStatuses", statusService.getAll());
        model.addAttribute("selectedStatus", statusService.getById(statusId));
        model.addAttribute("selectedTicket", ticketService.getByIdOrNew(ticketId));
        model.addAttribute("tickets", ticketService.getAllTicketsByStatusAndTeamAndActivity(
                statusService.getById(statusId), teamService.getById(teamId)));
        EasyDeskServer.LOGGER.info("Get 'support/ticket'");
        return "support/tickets";

    }
}
