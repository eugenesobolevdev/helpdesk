package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.support.models.Decline;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Контроллер обработки POST - запроса об отклонении обращения
 */
@Controller
public class DeclineController {
    private final TicketService ticketService;

    @Autowired
    public DeclineController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Обработчик POST - запроса о пеназначении группы поддержки.
     * @param model Модель данных.
     * @param redirectAttributes Аттрибуты переадресации.
     * @param currentUser Текущий авторизованный пользователь.
     * @param decline Вспомогательный класс, содержащий информацию о причине отклонения обращения,
     *                     полученный из фронта.
     * @return Страница переадресации.
     */
    @PostMapping("support/decline")
    public String addDeclineToTicket(Model model, RedirectAttributes redirectAttributes,
                                     @AuthenticationPrincipal User currentUser,
                                     @ModelAttribute(value = "decline")Decline decline) {
        Ticket currentTicket = ticketService.getByIdOrNew(decline.getTicketId());
        if (decline.getDecline().isEmpty()) {
            redirectAttributes.addFlashAttribute("declineIsEmptyError", "Заполните поле \"Причина отклонения\"");
            return "redirect:ticket?id=" + currentTicket.getId();
        }
        ticketService.messageToChat(currentTicket, "Ваше обращение отклонено. Причина:\n" + decline.getDecline());
        ticketService.writeToLog(currentTicket, currentUser, "the ticket has been declined.");
        currentTicket.setStatus(Status.DECLINED);
        currentTicket.setActive(false);
        ticketService.save(currentTicket);
        EasyDeskServer.LOGGER.info("Post 'support/decline'");
        return "redirect:ticket?id=" + currentTicket.getId();
    }
}
