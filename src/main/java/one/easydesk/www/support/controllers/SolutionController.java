package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.support.models.Solution;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Контроллер обработки POST - запроса о выполнении обращения
 */
@Controller
public class SolutionController {
    private final TicketService ticketService;

    @Autowired
    public SolutionController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Обработчик POST - запроса о пеназначении группы поддержки.
     * @param model Модель данных.
     * @param redirectAttributes Аттрибуты переадресации.
     * @param currentUser Текущий авторизованный пользователь.
     * @param solution Вспомогательный класс, содержащий информацию о результатах выполнения обращения,
     *                     полученный из фронта.
     * @return Страница переадресации.
     */
    @PostMapping("support/solution")
    public String addSolutionToTicket(Model model, RedirectAttributes redirectAttributes,
                                      @AuthenticationPrincipal User currentUser,
                                      @ModelAttribute(value = "solution") Solution solution) {
        Ticket currentTicket = ticketService.getByIdOrNew(solution.getTicketId());
        if (solution.getSolution().isEmpty()) {
            redirectAttributes.addFlashAttribute("solutionIsEmptyError", "Заполните поле \"Решение\"");
            return "redirect:ticket?id=" + currentTicket.getId();
        }
        ticketService.messageToChat(currentTicket, "Ваше обращение решено. Комментарий:\n" + solution.getSolution());
        ticketService.writeToLog(currentTicket, currentUser, "the ticket has been resolved.");
        currentTicket.setStatus(Status.DONE);
        currentTicket.setActive(false);
        ticketService.save(currentTicket);
        EasyDeskServer.LOGGER.info("Post 'support/solution'");
        return "redirect:ticket?id=" + currentTicket.getId();
    }
}
