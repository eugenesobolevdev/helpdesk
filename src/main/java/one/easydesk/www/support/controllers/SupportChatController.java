package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Контроллер переписки сотрудника поддержки с пользователем внутри обращения
 */
@Controller
public class SupportChatController {
    private final TicketService ticketService;

    @Autowired
    public SupportChatController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * POST - запрос на добавление сообщения в обращение.
     * @param ticket Целевое обращение, полученное из фронта.
     */
    @PostMapping("support/chat")
    public String addMessageToChat(Model model, @ModelAttribute(value = "id") Ticket ticket){
            Ticket ticketFromDB = ticketService.getByIdOrNew(ticket.getId());
            if (!ticket.getMessageToChat().isEmpty()){
                ticketService.messageToChat(ticketFromDB, ticket.getMessageToChat());
            }
        EasyDeskServer.LOGGER.info("Post 'support/chat'");
        return "redirect:ticket?id=" + ticket.getId();
    }
}
