package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import one.easydesk.www.support.models.AssignExpert;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Контроллер обработки POST - запроса о назначении специалиста поддержки
 */
@Controller
public class AssignExpertController {
    private final TicketService ticketService;
    private final UserService userService;

    @Autowired
    public AssignExpertController(TicketService ticketService, UserService userService) {
        this.ticketService = ticketService;
        this.userService = userService;
    }

    /**
     * Обработчик POST - запроса о назначении специалиста поддержки.
     * @param model Модель данных.
     * @param redirectAttributes Аттрибуты переадресации.
     * @param currentUser Текущий авторизованный пользователь.
     * @param assignExpert Вспомогательный класс, содержащий информацию о новом специалисте,
     *                     полученный из фронта.
     * @return Страница переадресации.
     */
    @PostMapping("support/assignexpert")
    public String addSolutionToTicket(Model model, RedirectAttributes redirectAttributes,
                                      @AuthenticationPrincipal User currentUser,
                                      @ModelAttribute(value = "assignExpert")AssignExpert assignExpert) {
        Ticket currentTicket = ticketService.getByIdOrNew(assignExpert.getTicketId());
        final User newExpert = userService.getUserById(assignExpert.getNewExpertId());
        final String oldExpertName = currentTicket.getExpert() == null ? "" : currentTicket.getExpert().getUsername();
        if (assignExpert.getNewExpertId() > 0) {
            if (assignExpert.getComment().isEmpty()){
                redirectAttributes.addFlashAttribute("commentIsEmptyError", "Необходим комментарий!");
                return "redirect:ticket?id=" + currentTicket.getId();
            }
            ticketService.writeToLog(currentTicket, currentUser, "old expert: \"" +
                    oldExpertName + "\", new expert: \"" +
                    newExpert.getUsername() + "\", comment: \"" +
                    assignExpert.getComment() + "\".");
            currentTicket.setExpert(newExpert);
            ticketService.save(currentTicket);
        }
        EasyDeskServer.LOGGER.info("Post 'support/assignexpert'");
        return "redirect:ticket?id=" + currentTicket.getId();
    }
}
