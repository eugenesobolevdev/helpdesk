package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Контроллер страницы со списком текущих обращений в работе у авторизованного пользователя.
 */
@Controller
public class SupportInWorkController {
    private final TicketService ticketService;

    @Autowired
    public SupportInWorkController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Get-запрос на страницу для работы с обращениями
     * @param currentUser - текущий авторизованный пользователь.
     * Список передаваемых в UI аттрибутов:
     * tickets - Список обращений в роботе у авторизованного пользователя.
     * currentExpertSupportTeams - Список групп поддержки текущего пользователя.
     * allSupportTeams - Список всех групп.
     */
    @GetMapping("/support/inwork")
    public String showTickets(Model model, @AuthenticationPrincipal User currentUser){
        model.addAttribute("tickets", ticketService.getAllActiveTicketsByExpert(currentUser));
        EasyDeskServer.LOGGER.info("Get 'support/inwork'");
        return "support/inwork";
    }
}
