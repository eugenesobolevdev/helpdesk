package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.admin.models.Team;
import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.support.models.AssignTeam;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Контроллер обработки POST - запроса о переназначении группы обращения
 */
@Controller
public class AssignTeamController {
    private final TicketService ticketService;
    private final TeamService teamService;

    @Autowired
    public AssignTeamController(TicketService ticketService, TeamService teamService) {
        this.ticketService = ticketService;
        this.teamService = teamService;
    }

    /**
     * Обработчик POST - запроса о переназначении группы поддержки.
     * Если сотрудник, у которого данное обращение находится в работе, состоит в новой группе, то оставляем
     * его, иначе обнуляем специалиста.
     * @param model Модель данных.
     * @param redirectAttributes Аттрибуты переадресации.
     * @param currentUser Текущий авторизованный пользователь.
     * @param assignTeam Вспомогательный класс, содержащий информацию о новой группе,
     *                     полученный из фронта.
     * @return Страница переадресации.
     */
    @PostMapping("support/reassignteam")
    public String addSolutionToTicket(Model model, RedirectAttributes redirectAttributes,
                                      @AuthenticationPrincipal User currentUser,
                                      @ModelAttribute(value = "assignTeam") AssignTeam assignTeam) {
        Ticket currentTicket = ticketService.getByIdOrNew(assignTeam.getTicketId());
        final Team newTeam = teamService.getById(assignTeam.getNewTeamId());
        final String oldTeamName = currentTicket.getCurrentTeam() == null ? "" : currentTicket.getCurrentTeam().getName();
        if (assignTeam.getNewTeamId() > 0) {
            if (assignTeam.getComment().isEmpty()){
                redirectAttributes.addFlashAttribute("commentIsEmptyError", "Необходим комментарий!");
                return "redirect:ticket?id=" + currentTicket.getId();
            }
            ticketService.writeToLog(currentTicket, currentUser, "old team: \"" +
                    oldTeamName + "\", new team: \"" +
                    newTeam.getName() + "\", comment: \"" +
                    assignTeam.getComment() + "\".");
            currentTicket.setCurrentTeam(newTeam);
            if (!newTeam.getUsers().contains(currentTicket.getExpert())){
                currentTicket.setExpert(null);
            }
            ticketService.save(currentTicket);
        }
        EasyDeskServer.LOGGER.info("Post 'support/assignteam'");
        return "redirect:ticket?id=" + currentTicket.getId();
    }
}
