package one.easydesk.www.support.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.user.models.Attach;
import one.easydesk.www.user.models.Ticket;
import one.easydesk.www.user.services.AttachService;
import one.easydesk.www.user.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Контроллер для прикрепления файлов к обращениям и их отображения со стороны специалистов поддержки
 */
@Controller
@CrossOrigin("http://localhost:8081")
class SupportAttachController {
    private final AttachService attachService;
    private final TicketService ticketService;

    @Autowired
    public SupportAttachController(AttachService attachService, TicketService ticketService) {
        this.attachService = attachService;
        this.ticketService = ticketService;
    }

    /**
     * Извлекает файл из БД и предоставляет его специалисту.
     * @param id ID файла.
     * @return
     */
    @GetMapping("/support/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable Long id) {
        Attach attach = attachService.getFile(id);
        EasyDeskServer.LOGGER.info("Get attach {}", attach.getName());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attach.getName() + "\"")
                .body(attach.getData());
    }

    /**
     * Загружает файл в БД. При этом владельцем файла указывается создатель обращения.
     * @param ticket Целевое обращение.
     * @param file Файл.
     */
    @PostMapping("support/upload")
    public String uploadAttach(Model model, @AuthenticationPrincipal User currentUser,
                                @ModelAttribute(value = "ticket") Ticket ticket,
                                @RequestPart("file") MultipartFile file){
            ticket = ticketService.getByIdOrNew(ticket.getId());
            try {
                if (!file.isEmpty()) attachService.store(file, ticket, ticket.getApplicant());
                EasyDeskServer.LOGGER.info("Uploaded the file successfully: {}",file.getOriginalFilename());
                ticketService.writeToLog(ticket, currentUser, "attach+: " + file.getOriginalFilename());
            } catch (Exception e) {
                EasyDeskServer.LOGGER.error("Could not upload the file: {}!", file.getOriginalFilename());
            }
        return "redirect:ticket?id=" + ticket.getId();
    }
}
