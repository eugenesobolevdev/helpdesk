package one.easydesk.www.authentication.models;

import lombok.Getter;
import lombok.Setter;
import one.easydesk.www.admin.models.Team;
import one.easydesk.www.user.models.Attach;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * Сущность, представляющая собой учетную запись пользователя.
 * Имеет связь Many-to-many с сущностью Team - группой поддержки,
 * а также связь Many-to-many с сущностью Role - ролью пользователя
 */
@Getter
@Setter
@Entity
@Table(name = "t_users")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 3, max = 30, message = "Логин должен быть не менее 3 и не более 30 символов.")
    @Column(name = "username")
    private String username;

    @Size(min = 3, message = "Пароль должен быть не менее 3 символов.")
    @Column(name = "password")
    private String password;

    @Transient
    private String passwordConfirm;

    @Transient
    private String newPassword;

    @Transient
    private String oldPassword;

    @Transient
    private boolean support;

    @Transient
    private boolean admin;

    @Column(name = "fio")
    private String fio;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "department")
    private String department;

    @Size(min = 3, message = "E-mail должен быть не менее 3 символов.")
    @Email(message = "Проверьте правильность ввода e-mail.")
    @Column(name = "email")
    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Team> teams = new HashSet<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Attach> attach = new ArrayList<>();

    public User() {
        this.newPassword=  "";
        this.oldPassword = "";
        this.enabled = true;
    }

    public User(String username, String password, Set<Role> roles) {
        this.newPassword = "";
        this.oldPassword = "";
        this.username = username;
        this.password = password;
        this.passwordConfirm = password;
        this.enabled = true;
        this.roles = roles;
    }

    public boolean containRoleSupport(){
        return roles.contains(new Role(2L, "ROLE_SUPPORT"));
    }

    public boolean containRoleAdmin(){
        return roles.contains(new Role(3L, "ROLE_ADMIN"));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) && password.equals(user.password) && Objects.equals(passwordConfirm, user.passwordConfirm) && Objects.equals(fio, user.fio) && Objects.equals(department, user.department) && Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, passwordConfirm, fio, department, email);
    }
}
