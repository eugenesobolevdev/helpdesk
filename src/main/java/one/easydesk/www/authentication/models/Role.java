package one.easydesk.www.authentication.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * Сущность, представляющая собой роль пользователя.
 * Имеет связть Many-to-many с сущностью User - учетной записи пользователя.
 */
@Getter
@Setter
@Entity
@Table(name = "t_roles")
public class Role implements GrantedAuthority{
    @Transient
    public static final Role ROLE_USER = new Role(1L, "ROLE_USER");
    @Transient
    public static final Role ROLE_SUPPORT = new Role(2L, "ROLE_SUPPORT");
    @Transient
    public static final Role ROLE_ADMIN = new Role(3L, "ROLE_ADMIN");

    @Id
    private Long id;

    @Column(name = "role")
    private String role;

    @Transient
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "roles")
    private Set<User> users = new java.util.LinkedHashSet<>();

    public Role() {
    }

    public Role(Long id) {
        this.id = id;
    }

    public Role(Long id, String role) {
        this.id = id;
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role1 = (Role) o;
        return role.equals(role1.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(role);
    }
}
