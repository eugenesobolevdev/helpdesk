package one.easydesk.www.authentication.repositories;


import one.easydesk.www.authentication.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Интерфейс репозитория для работы с БД с ролями пользователей
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
