package one.easydesk.www.authentication.repositories;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс репозитория для работы с БД с учетными записями пользователей
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    List<User> findUsersByRolesAndTeamsNotContainsOrderByUsernameAsc(Role role, Team team);
    List<User> findUsersByRolesAndTeamsContainsAndIdIsNotOrderByFio(Role role, Team team, Long exceptId);
    List<User> findAllByOrderByUsernameAsc();
}
