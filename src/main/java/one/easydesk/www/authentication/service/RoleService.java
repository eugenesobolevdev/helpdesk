package one.easydesk.www.authentication.service;

import one.easydesk.www.authentication.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Сервис, реализующий связь между UI и БД для ролей пользователей
 */
@Service
public class RoleService { @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
}
