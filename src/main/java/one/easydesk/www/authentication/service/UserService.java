package one.easydesk.www.authentication.service;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * Сервис, реализующий связь между UI и БД для учетных записей пользователей
 */
@Service
public class UserService implements UserDetailsService {
    @PersistenceContext
    private EntityManager em;

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Проверяет существование пользователя в БД
     * @param username имя учетной записи
     * @return сущность учетной записи
     * @throws UsernameNotFoundException бросает исключение в случае, пользователь не найден
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

    /**
     * Возвращает сущность пользователя по ID
     * @param userId ID
     * @return User
     */
    public User getUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    /**
     * @return список всех пользователей
     */
    public List<User> getAll() {
        return userRepository.findAllByOrderByUsernameAsc();
    }

    /**
     * Добавляет новую учетную запись пользователя в БД
     * @param user сущность УЗ
     * @return результат добавления
     */
    public boolean addUser(User user) {
        User userFromDB = userRepository.findByUsername(user.getUsername());
        if (userFromDB != null) {
            return false;
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    /**
     * Сохраняет измененную УЗ пользователя в БД
     * @param user сущность УЗ
     */
    public void editUser(User user){
        if (!user.getNewPassword().isEmpty()) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getNewPassword()));
        }
        userRepository.save(user);
    }

    /**
     * Удаляет УЗ из БД по ID
     * @param userId ID
     * @return результат удаления
     */
    public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    /**
     * Возвращает список всех УЗ с заданной ролью в заданной команде
     * @param role Роль
     * @param team Команда
     * @return список УЗ
     */
    public List<User> showAllByRoleAndTeam(Role role, Team team){
        return userRepository.findUsersByRolesAndTeamsNotContainsOrderByUsernameAsc(role, team);
    }

    /**
     * Возвращает список всех УЗ с заданной ролью в заданной команде, кроме заданного пользователя
     * @param role Роль
     * @param team Команда
     * @param user Пользователь
     * @return список УЗ
     */
    public List<User> showAllByRoleExceptOne(Role role, Team team, User user){
        return userRepository.findUsersByRolesAndTeamsContainsAndIdIsNotOrderByFio(role, team, user.getId());
    }

    /**
     * @return Количество УЗ в БД.
     */
    public Long getCount(){
        return userRepository.count();
    }
}
