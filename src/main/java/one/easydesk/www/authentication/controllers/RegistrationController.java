package one.easydesk.www.authentication.controllers;

import one.easydesk.www.EasyDeskServer;
import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Collections;

/**
 * Контроллер страницы регистрации нового пользователя
 */
@Controller
public class RegistrationController {
    final private UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Отображает страницу регистрации
     * @param model модель данных
     * @return html-страница
     */
    @GetMapping("/reg")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        EasyDeskServer.LOGGER.info("Get 'reg/'");
        return "reg";
    }

    /**
     * Принимает данные нового пользователя, проверяет их и добавляет в БД
     * @param userForm сущность нового пользователя
     * @param bindingResult результат валидации
     * @param model модель данных
     * @return возвращает html-страницу, сообщающую об успешной регистрации
     */
    @PostMapping("/reg")
    public String addUser(@ModelAttribute(value = "userForm") @Valid User userForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("registerFormErrors", bindingResult.getFieldErrors());
            return "reg";
        }
        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())){
            model.addAttribute("passwordError", "Пароли не совпадают");
            return "reg";
        }
        userForm.setRoles(Collections.singleton(Role.ROLE_USER));
        if (!userService.addUser(userForm)){
            model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
            return "reg";
        }
        EasyDeskServer.LOGGER.info("Post 'reg/'");
        return "regsuccess";
    }
}
