package one.easydesk.www;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyDeskServer {
	public static final Logger LOGGER = LoggerFactory.getLogger(EasyDeskServer.class);
	public static void main(String[] args) {
		SpringApplication.run(EasyDeskServer.class, args);
	}
}
