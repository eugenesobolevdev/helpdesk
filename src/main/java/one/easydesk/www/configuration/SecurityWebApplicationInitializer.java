package one.easydesk.www.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.stereotype.Component;

/**
 * Компонент spring security.
 */
@Component
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
