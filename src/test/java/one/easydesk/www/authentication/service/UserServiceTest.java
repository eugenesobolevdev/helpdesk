package one.easydesk.www.authentication.service;

import one.easydesk.www.authentication.models.Role;
import one.easydesk.www.authentication.models.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Collection;
import java.util.Collections;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тесты сервиса для работы с учетными записями.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;
    private User testUser;

    @Before
    public void setUp() throws Exception {
        testUser = new User("TestName$2a$10$qsB0ZY1", "$2a$10$qsB0ZY1usorO8MSSzggmeuOhXWGXx.9m2JaDUWOp7vB2coZ3T39f.", Collections.singleton(Role.ROLE_USER));
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Тест метода, который возвращает UserDetails по имени пользователя или исключение UsernameNotFoundException.
     */
    @Test
    public void loadUserByUsername() {
        exceptionRule.expect(UsernameNotFoundException.class);
        userService.loadUserByUsername("TestName$2a$10$qsB0ZY1");
        assertTrue(userService.addUser(testUser));
        userService.deleteUser(testUser.getId());
    }

    /**
     * Тест метода, который возвращает пользователя по ID
     */
    @Test
    public void getUserById() {
        assertEquals("admin", userService.getUserById(1L).getUsername());
    }

    /**
     * Тест метода, который возвращает список всех пользователей
     */
    @Test
    public void allUsers() {
        Collection<User> users = userService.getAll();
        assertEquals(users.size(), userService.getCount());
    }

    /**
     * Тест метода, который добавляет нового пользователя в БД
     */
    @Test
    public void addUser() {
        assertTrue(userService.addUser(testUser));
        assertFalse(userService.addUser(testUser));
        userService.deleteUser(testUser.getId());
    }

    /**
     * Тест метода, который редактирует пользователя в БД
     */
    @Test
    public void editUser() {
        userService.addUser(testUser);
        User editedUser = userService.getUserById(testUser.getId());
        editedUser.setFio("TEST FIO");
        userService.editUser(editedUser);
        assertEquals("TEST FIO", userService.getUserById(testUser.getId()).getFio());
        userService.deleteUser(editedUser.getId());
    }

    /**
     * Тест метода, который удаляет пользователя по ID
     */
    @Test
    public void deleteUser() {
        userService.addUser(testUser);
        assertEquals("TestName$2a$10$qsB0ZY1", userService.loadUserByUsername("TestName$2a$10$qsB0ZY1").getUsername());
        userService.deleteUser(testUser.getId());
        exceptionRule.expect(UsernameNotFoundException.class);
        userService.loadUserByUsername("TestName$2a$10$qsB0ZY1");
    }

    /**
     * Тест метода, который возвращает количество УЗ в БД
     */
    @Test
    public void getCount() {
        Collection<User> users = userService.getAll();
        assertEquals(users.size(), userService.getCount());
    }
}