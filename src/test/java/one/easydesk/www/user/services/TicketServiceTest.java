package one.easydesk.www.user.services;

import one.easydesk.www.admin.services.TeamService;
import one.easydesk.www.authentication.models.User;
import one.easydesk.www.authentication.service.UserService;
import one.easydesk.www.user.models.Status;
import one.easydesk.www.user.models.Ticket;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

/**
 * Тесты сервиса для работы с обращениями.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketServiceTest {
    @Autowired
    private TicketService ticketService;
    @Autowired
    private UserService userService;
    @Autowired
    private TeamService teamService;

    private Ticket testTicket;
    private User testUser;

    @Before
    public void setUp() throws Exception {
        testUser = userService.getUserById(1L);
        testTicket = new Ticket(testUser);
        testTicket.setExpert(testUser);
        testTicket.setStatus(Status.REGISTERED);
        testTicket.setCurrentTeam(teamService.getById(1L));
        testTicket.setApplicant(testUser);
        ticketService.save(testTicket);
    }

    @After
    public void tearDown() throws Exception {
        ticketService.deleteTicketFromDB(testTicket.getId());
    }

    /**
     * Тест метода, который возвращает все не архивные обращения заданного пользователя
     */
    @Test
    public void getNonArchivedTicketsByUser() {
        ArrayList<Ticket> tickets = (ArrayList<Ticket>) ticketService.getNonArchivedTicketsByUser(testUser);
        Assert.assertTrue(tickets.contains(testTicket));
    }

    /**
     * Тест метода, который возвращает все архивные обращения заданного пользователя
     */
    @Test
    public void getArchivedTicketsByUser() {
        ticketService.archiveTicket(testUser, testTicket.getId());
        ArrayList<Ticket> tickets = (ArrayList<Ticket>) ticketService.getArchivedTicketsByUser(testUser);
        Assert.assertTrue(tickets.contains(testTicket));
    }

    /**
     * Тест метода, который переводит обращение в статус "В архиве"
     */
    @Test
    public void archiveTicket() {
        ticketService.archiveTicket(testUser, testTicket.getId());
        Assert.assertEquals(Status.ARCHIVED, ticketService.getById(testTicket.getId()).getStatus());
    }

    /**
     * Тест метода, который переводит обращение в статус "Удалено"
     */
    @Test
    public void deleteTicket() {
        ticketService.deleteTicket(testUser, testTicket.getId());
        Assert.assertEquals(Status.DELETED, ticketService.getById(testTicket.getId()).getStatus());
    }

    /**
     * Тест метода, который возвращает обращение по ID, если пользователь совпадает
     */
    @Test
    public void getByUserAndId() {
        Assert.assertEquals(testTicket, ticketService.getByUserAndId(testUser, testTicket.getId()));
    }

    /**
     * Тест метода, который возвращает обращение по ID
     */
    @Test
    public void getById() {
        Assert.assertEquals(testTicket, ticketService.getById(testTicket.getId()));
    }

    /**
     * Тест метода, который добавляет сообщение в чат
     */
    @Test
    public void messageToChat() {
        ticketService.messageToChat(testTicket, "TEST$2a$10$qsB0ZY1");
        Assert.assertTrue(ticketService.getById(testTicket.getId()).getChat().contains("TEST$2a$10$qsB0ZY1"));
    }

    /**
     * Тест метода, который добавляет запись в лог
     */
    @Test
    public void writeToLog() {
        ticketService.writeToLog(testTicket, testUser, "TEST$2a$10$qsB0ZY1");
        Assert.assertTrue(ticketService.getById(testTicket.getId()).getLog().contains("TEST$2a$10$qsB0ZY1"));
    }

    /**
     * Тест метода, который удаляет запись из БД по ID
     */
    @Test
    public void deleteById() {
        Assert.assertTrue(ticketService.getAll().contains(testTicket));
        ticketService.deleteById(testTicket.getId());
        Assert.assertFalse(ticketService.getAll().contains(testTicket));
    }

    /**
     * Тест метода, который сохраняет изменения записи в БД
     */
    @Test
    public void save() {
        Assert.assertNull(ticketService.getById(testTicket.getId()).getShortDescription());
        testTicket.setShortDescription("TEST$2a$10$qsB0ZY1");
        ticketService.save(testTicket);
        Assert.assertEquals("TEST$2a$10$qsB0ZY1", ticketService.getById(testTicket.getId()).getShortDescription());
    }
}