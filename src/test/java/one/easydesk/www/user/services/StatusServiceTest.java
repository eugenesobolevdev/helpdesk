package one.easydesk.www.user.services;

import one.easydesk.www.admin.models.Team;
import one.easydesk.www.user.models.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тесты сервиса для работы со статусами обращений.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StatusServiceTest {
    @Autowired
    private StatusService statusService;

    @Test
    public void getAll() {
        Collection<Status> statuses = statusService.getAll();
        assertEquals(7, statuses.size());
    }

    @Test
    public void getById() {
        assertEquals(Status.REGISTERED, statusService.getById(1L));
    }
}