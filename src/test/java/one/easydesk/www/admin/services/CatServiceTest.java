package one.easydesk.www.admin.services;

import one.easydesk.www.admin.models.Category;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тесты сервиса категорий поддержки
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CatServiceTest {
    @Autowired
    private CatService catService;

    @Autowired
    private TeamService teamService;

    /**
     * Тест метода, возвращаюшего список всех категорий
     */
    @Test
    public void getAll() {
        Collection<Category> categories = catService.getAll();
        assertEquals(categories.size(), catService.getCount());
    }

    /**
     * Тест метода, возвращаюшего категорию по ID
     */
    @Test
    public void getById() {
        final Category testCat = new Category("Test", "Test", teamService.getById(1L));
        catService.edit(testCat);
        assertEquals(catService.getById(testCat.getId()), testCat);
        catService.deleteById(testCat.getId());
    }

    /**
     * Тест метода, удаляюшего категорию по ID
     */
    @Test
    public void deleteById() {
        final Category deletedCat = new Category("Test", "Test", teamService.getById(1L));
        catService.edit(deletedCat);
        catService.deleteById(deletedCat.getId());
        Assert.assertNotEquals(deletedCat, catService.getById(deletedCat.getId()));
    }

    /**
     * Тест метода, изменяющего существующую или создающего новую запись в БД
     */
    @Test
    public void edit() {
        Category editedCat = new Category("Test", "Test", teamService.getById(1L));
        catService.edit(editedCat);
        editedCat.setName("New Name");
        catService.edit(editedCat);
        assertEquals("New Name", catService.getById(editedCat.getId()).getName());
        catService.deleteById(editedCat.getId());
    }

    /**
     * Тест метода, возвращаюшего количество категорий в БД
     */
    @Test
    public void getCount() {
        Collection<Category> categories = catService.getAll();
        assertEquals(categories.size(), catService.getCount());
    }

    /**
     * Тест метода, возвращаюшего группу поддержки по-умолчанию для заданной по ID категории
     */
    @Test
    public void getDefaultTeamById() {
        Category testCat = new Category("Test", "Test", teamService.getById(1L));
        catService.edit(testCat);
        assertEquals(1L, catService.getDefaultTeamById(testCat.getId()).getId());
        catService.deleteById(testCat.getId());
    }
}