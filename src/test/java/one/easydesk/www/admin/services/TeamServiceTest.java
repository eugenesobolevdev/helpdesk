package one.easydesk.www.admin.services;

import one.easydesk.www.admin.models.Team;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тесты сервиса групп поддержки
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TeamServiceTest {
    @Autowired
    TeamService teamService;

    /**
     * Тест метода, возвращаюшего список всех команд
     */
    @Test
    public void getAll() {
        Collection<Team> teams = teamService.getAll();
        assertEquals(teams.size(), teamService.getCount());
    }

    /**
     * Тест метода, возвращаюшего команду по ID
     */
    @Test
    public void getById() {
        final Team testTeam = new Team("Test", "Test");
        teamService.edit(testTeam);
        assertEquals(teamService.getById(testTeam.getId()), testTeam);
        teamService.deleteById(testTeam.getId());
    }

    /**
     * Тест метода, удаляющего команду по ID
     */
    @Test
    public void deleteById() {
        final Team deletedTeam = new Team("Test", "Test");
        teamService.edit(deletedTeam);
        teamService.deleteById(deletedTeam.getId());
        Assert.assertNotEquals(deletedTeam, teamService.getById(deletedTeam.getId()));
    }

    /**
     * Тест метода, изменяющего существующую или создающего новую запись в БД
     */
    @Test
    public void edit() {
        final Team editedTeam = new Team("Test", "Test");
        teamService.edit(editedTeam);
        editedTeam.setName("New Name");
        teamService.edit(editedTeam);
        assertEquals("New Name", teamService.getById(editedTeam.getId()).getName());
        teamService.deleteById(editedTeam.getId());
    }

    /**
     * Тест метода, возвращаюшего количество команд в БД
     */
    @Test
    public void getCount() {
        Collection<Team> teams = teamService.getAll();
        assertEquals(teams.size(), teamService.getCount());
    }

    /**
     * Тест метода, возращающего список всех команд, кроме заданной
     */
    @Test
    public void getAllTeamsExceptOne() {
        ArrayList<Team> allTeams = teamService.getAll();
        allTeams.remove(0);
        ArrayList<Team> allTeamsExceptOne = (ArrayList<Team>) teamService.getAllTeamsExceptOne(1L);
        assertEquals(allTeams, allTeamsExceptOne);
    }
}